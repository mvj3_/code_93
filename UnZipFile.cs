//对zip文件的解压缩操作
//UnZipFile()方法，传入的第一个参数为欲解压的文件位置（解压缩位置LocalFolder中新建一个文件夹，可以根据实际需求进行修改），第二个参数为压缩文件的字节数组，这个数组可能来源于网络下载或者其他独立存储空间中的文件。
public async Task<StorageFolder> UnZipFile(string folderName, byte[] data)
        {
            StorageFolder unZipfolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);

            var stream = new MemoryStream(data);
            var zipArchive = new ZipArchive(stream, ZipArchiveMode.Read);
            foreach (var zipArchiveEntry in zipArchive.Entries)
            {
                if (!String.IsNullOrEmpty(zipArchiveEntry.FullName))
                {
                    if (!zipArchiveEntry.FullName.EndsWith("/"))
                    {
                        string fileName = zipArchiveEntry.FullName.Replace("/", "\\");
                        using (Stream fileData = zipArchiveEntry.Open())
                        {
                            StorageFile newFile = await unZipfolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
                            using (IRandomAccessStream newFileStream = await newFile.OpenAsync(FileAccessMode.ReadWrite))
                            {
                                using (Stream s = newFileStream.AsStreamForWrite())
                                {
                                    await fileData.CopyToAsync(s);
                                    await s.FlushAsync();
                                    s.Dispose();
                                }
                                newFileStream.Dispose();
                            }
                        }
                    }
                }
            }
            return unZipfolder;
        }