//对文件夹的压缩（如果需要压缩单个文件，代码可从下面的代码中提取）        
//ZipFolder()方法为将一个文件夹极其子文件夹与文件压缩成一个ZIP文件，具体过程为递归调用AddZipFolderToEntry。
public async Task<byte[]> ZipFolder(StorageFolder folder)
        {
            using (var zipMemoryStream = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(zipMemoryStream, ZipArchiveMode.Create))
                {
                    await AddZipFolderToEntry(zipArchive, folder, "");
                }
                return zipMemoryStream.ToArray();
            }
        }

        private async Task<bool> AddZipFolderToEntry(ZipArchive zipArchive, StorageFolder folder, string entryFirst)
        {
            IReadOnlyList<StorageFile> filesToCompress = await folder.GetFilesAsync();

            foreach (StorageFile fileToCompress in filesToCompress)
            {
                byte[] buffer = (await FileIO.ReadBufferAsync(fileToCompress)).ToArray();
                ZipArchiveEntry entry = zipArchive.CreateEntry(entryFirst + fileToCompress.Name);
                using (Stream entryStream = entry.Open())
                {
                    await entryStream.WriteAsync(buffer, 0, buffer.Length);
                }
            }
            var childrenFolder = await folder.GetFoldersAsync();
            foreach (var storageFolder in childrenFolder)
            {
                await AddZipFolderToEntry(zipArchive, storageFolder, entryFirst + storageFolder.Name + "/");
            }

            return true;
        }